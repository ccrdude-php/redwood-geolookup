<?php
/**
 * Specific class to look up country information using Pear libraries.
 *
 * @package    Redwood
 * @subpackage GeoLookup
 * @author     Patrick Kolla-ten Venne
 * @copyright  2023 Patrick Kolla-ten Venne
 * @license    MIT
 */

declare(strict_types=1);

namespace RedWood\GeoLookup;

use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/../includes/class-pearlookup.php';

class PearLookupTest extends TestCase
{
	public function testGermanIP()
	{
		$pear = new PearLookup(__DIR__ . '/../db');
		if(!$pear->isSupported()) {
			print_r($pear->DebugInformation);			
		}
		$this->assertTrue($pear->isSupported());
		$a = $pear->lookupCountry('45.155.143.146');
		$this->assertEquals('DE', $a->code);
		print_r($pear->DebugInformation);
	}
}