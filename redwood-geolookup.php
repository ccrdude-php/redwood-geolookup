<?php
/**
 * RedWood GeoLookup
 *
 * @package    Redwood
 * @subpackage GeoLookup
 * @author     Patrick Kolla-ten Venne
 * @copyright  2023 Patrick Kolla-ten Venne
 * @license    MIT
 *
 * @wordpress-plugin
 * Plugin Name: Reedwood GeoLookup
 * Plugin URI:  https://gitlab.com/ccrdude-php/redwood-geolookup
 * Description: Allows to lookup an approximate location using the visitors IP address.
 * Version:     1.1.0
 * Author:      Patrick Kolla-ten Venne
 * Author URI:  https://www.ccrdude.net/
 * License:     MIT
 * Text Domain: redwood-geolookup
 */

namespace RedWood\GeoLookup;

require_once __DIR__ . '/includes/class-ip.php';
require_once __DIR__ . '/includes/class-pearlookup.php';
require_once __DIR__ . '/includes/class-pecllookup.php';
require_once __DIR__ . '/includes/class-maxmindv1lookup.php';
require_once __DIR__ . '/includes/class-maxmindv2lookup.php';
if (is_admin()) {
	require_once __DIR__ . '/includes/class-adminpage.php';
}

/*
Copyright 2023 Patrick Kolla-ten Venne

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

if (is_admin()) {
	new AdminPage();
}
