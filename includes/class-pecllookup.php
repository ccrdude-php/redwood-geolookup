<?php

/**
 * Specific class to look up country information using PECL libraries.
 *
 * @package    Redwood
 * @subpackage GeoLookup
 * @author     Patrick Kolla-ten Venne
 * @copyright  2023 Patrick Kolla-ten Venne
 * @license    MIT
 */

namespace RedWood\GeoLookup;

include_once 'Net/GeoIP.php';

class PECLLookup
{
    public $DebugInformation = [];

    public function __construct()
    {
    }

    public function isSupported(): bool
    {
        $this->DebugInformation['NameFunctionExists']
            = json_encode((function_exists('geoip_country_name_by_name')));
        $this->DebugInformation['CodeFunctionExists']
            = json_encode((function_exists('geoip_country_code_by_name')));
        $bIsSupported = ((function_exists('geoip_country_code_by_name'))
            && (function_exists('geoip_country_name_by_name')));
        $this->DebugInformation['IsSupported']
            = json_encode($bIsSupported);
        return $bIsSupported;
    }

    public function lookupCountry($IPAddress): object
    {
		$a = array('code' => '', 'name' => '', 'success' => false, 'source' => 'pecl', 'currency' => '');
        if (!function_exists('geoip_country_name_by_name')) {
            $a['errorCode'] = 'Function geoip_country_name_by_name missing.';
            $this->DebugInformation['ErrorCode'] = $a['errorCode'];
            return (object)$a;
        }
        if (!function_exists('geoip_country_code_by_name')) {
            $a['errorCode'] = 'Function geoip_country_code_by_name missing.';
            $this->DebugInformation['ErrorCode'] = $a['errorCode'];
            return (object)$a;
        }
        try {
            $a['code'] = geoip_country_code_by_name($IPAddress);
            $a['name'] = geoip_country_name_by_name($IPAddress);
            $a['success'] = true;
            $this->DebugInformation['CountryCode'] = $a['code'];
            $this->DebugInformation['CountryName'] = $a['name'];
            $this->DebugInformation['DBFilename'] = geoip_db_filename(GEOIP_COUNTRY_EDITION);
            $this->DebugInformation['DBInfo'] = geoip_database_info();
        } catch (\Exception $e) {
            // Handle exception
            $a['errorCode'] = $e->getMessage();
            $this->DebugInformation['ErrorCode'] = $a['errorCode'];
        }
        return (object)$a;
    }
}
