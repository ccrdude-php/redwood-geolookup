<?php

/**
 * Main class to look up country information.
 *
 * @package    Redwood
 * @subpackage GeoLookup
 * @author     Patrick Kolla-ten Venne
 * @copyright  2023 Patrick Kolla-ten Venne
 * @license    MIT
 */

namespace RedWood\GeoLookup;

class IP
{
	public $DebugInformation = [];

	public function __construct()
	{
	}

	/**
	 * @see http://www.aph.com/community/holidays/list-of-european-countries-which-use-the-euro-currency/
	 */
	protected function getCurrencyForCountryCode(string $CountryCode, string $Default = 'EUR', array $RequestedCurrencies = []): string
	{
		$CountryCode = strtoupper($CountryCode);
		$aCountryCurrencies = array(
			'US' => 'USD',
			'UK' => 'GBP',
			// EU countries using the euro
			'AT' => 'EUR',
			'BE' => 'EUR',
			'CY' => 'EUR',
			'EE' => 'EUR',
			'FI' => 'EUR',
			'FR' => 'EUR',
			'DE' => 'EUR',
			'GR' => 'EUR',
			'IE'	=> 'EUR',
			'IT'	=> 'EUR',
			'LV'	=> 'EUR',
			'LT'	=> 'EUR',
			'LU'	=> 'EUR',
			'MT'	=> 'EUR',
			'NL'	=> 'EUR',
			'PT'	=> 'EUR',
			'ES'	=> 'EUR',
			'SI'	=> 'EUR',
			'SK'	=> 'EUR',
			// Non-EU countries using the euro have 2 countries Kosovo and Montanegro. But GeOIP detect as "Serbia and Montenegro"
			'CS'	=> 'EUR',
			'ME'	=> 'EUR',
			// Microstates using the euro
			'AD'	=> 'EUR',
			'MC'	=> 'EUR',
			'SM'	=> 'EUR',
			'VA'	=> 'EUR',
			// Switzerland… Note that the Euro is a foreign currency in Switzerland, but will be accepted. However, change will be in Swiss francs.
			'CH'	=> 'EUR',
			// NOTE: You can add more below, refer link http://dev.maxmind.com/geoip/legacy/codes/iso3166/ for the EU code refer http://dev.maxmind.com/geoip/legacy/codes/eu_country_list/
			// reference from http://www.aph.com/community/holidays/list-of-european-countries-which-use-the-euro-currency/
			//Anas_xrt - CloudRambo.com
			'KR' => 'KRW',
		);
		if (!array_key_exists($CountryCode, $aCountryCurrencies)) {
			return $Default;
		}
		$sCurrency = $aCountryCurrencies[$CountryCode];
		if(!in_array($sCurrency, $RequestedCurrencies)) {
			return $Default;
		}
		return $sCurrency;
	}

	public function lookupCountry(string $IPAddress, string $DefaultCurrency = 'EUR', array $RequestedCurrencies = []): object
	{
		$a = array('code' => '', 'name' => '', 'success' => false);
		$mm2 = new MaxMindV2Lookup(__DIR__ . '/../db');
		if ($mm2->isSupported()) {
			$data = $mm2->lookupCountry($IPAddress);
			$this->DebugInformation['maxmindv2'] = $mm2->DebugInformation;
			if ($data->success) {
				$data->currency = $this->getCurrencyForCountryCode($data->code, $DefaultCurrency, $RequestedCurrencies);
				return $data;
			}
		}
		$pecl = new PECLLookup();
		if ($pecl->isSupported()) {
			$data = $pecl->lookupCountry($IPAddress);
			$this->DebugInformation['pecl'] = $mm2->DebugInformation;
			if ($data->success) {
				$data->currency = $this->getCurrencyForCountryCode($data->code, $DefaultCurrency, $RequestedCurrencies);
				return $data;
			}
		}
		$pear = new PearLookup(__DIR__ . '/../db');
		if ($pear->isSupported()) {
			$data = $pear->lookupCountry($IPAddress);
			$this->DebugInformation['pear'] = $mm2->DebugInformation;
			if ($data->success) {
				$data->currency = $this->getCurrencyForCountryCode($data->code, $DefaultCurrency, $RequestedCurrencies);
				return $data;
			}
		}
		return (object)$a;
	}
}
