<?php

/**
 * Specific class to look up country information using Pear libraries.
 *
 * @package    Redwood
 * @subpackage GeoLookup
 * @author     Patrick Kolla-ten Venne
 * @copyright  2023 Patrick Kolla-ten Venne
 * @license    MIT
 */

namespace RedWood\GeoLookup;


class AdminPage
{
    protected $IP = '45.155.143.146';

    /**
     * Start up
     */
    public function __construct()
    {
        $this->IP = $_SERVER['REMOTE_ADDR'];
        add_action('admin_menu', array($this, 'addPluginPage'));
        add_action('admin_init', array($this, 'pageInit'));
    }

    /**
     * Add options page
     */
    public function addPluginPage()
    {
        add_management_page(
            'RedWood GeoLookup',
            'RedWood GeoLookup',
            'manage_options',
            'geolookup',
            array($this, 'createAdminPage'),
            9999
        );
    }

    protected function getPEARTest(): string
    {
        $ret = '<h3>PEAR lookup</h3><pre>';
        $pear = new PearLookup(__DIR__ . '/../db');
        if ($pear->isSupported()) {
            $pear->lookupCountry($this->IP);
        }
        $ret .= print_r($pear->DebugInformation, true);
        $ret .= '</pre>';
        return $ret;
    }

    protected function getPECLTest(): string
    {
        $ret = '<h3>PECL lookup</h3><pre>';
        $pear = new PECLLookup();
        if ($pear->isSupported()) {
            $pear->lookupCountry($this->IP);
        }
        $ret .= print_r($pear->DebugInformation, true);
        $ret .= '</pre>';
        return $ret;
    }

    protected function getMaxMindV1Test(): string
    {
        $ret = '<h3>MaxMind GeoIP v1 lookup</h3><pre>';
        $pear = new MaxMindV1Lookup(__DIR__ . '/../db');
        if ($pear->isSupported()) {
            $pear->lookupCountry($this->IP);
        }
        $ret .= print_r($pear->DebugInformation, true);
        $ret .= '</pre>';
        return $ret;
    }

    protected function getMaxMindV2Test(): string
    {
        $ret = '<h3>MaxMind GeoIP v2 lookup</h3><pre>';
        $pear = new MaxMindV2Lookup(__DIR__ . '/../db');
        if ($pear->isSupported()) {
            $pear->lookupCountry($this->IP);
        }
        $ret .= print_r($pear->DebugInformation, true);
        $ret .= '</pre>';
        return $ret;
    }

    protected function getIPTest(): string
    {
        $ret = '<h3>IP lookup</h3><pre>';
        $ip = new IP();
        $ret .= print_r($ip->lookupCountry($this->IP, 'EUR', ['EUR', 'USD']), true);
        $ret .= '</pre>';
        return $ret;
    }

    protected function getTests(): string
    {
        $ret = '<h2>Tests</h2>';
        $ret .= $this->getIPTest();
        $ret .= $this->getPEARTest();
        $ret .= $this->getPECLTest();
        $ret .= $this->getMaxMindV1Test();
        $ret .= $this->getMaxMindV2Test();
        return $ret;
    }

    /**
     * Undocumented function
     *
     * @return string
     * @link https://us.battle.net/support/en/article/7871
     * @link https://www.microsoft.com/en-us/download/details.aspx?id=56519
     */
    protected function getTestMatrix(): string
    {
        $ret = '<h2>Test Matrix</h2>';
        $ret .= <<<FOOBAR
        <table style="border: 1px solid gainsboro; padding: 1em;">
            <thead>
                <tr>
                    <th>IP</th>
                    <th>Location</th>
                    <th>Context</th>
                    <th>Currency</th>
                    <th>Pear</th>
                    <th>Pecl</th>
                    <th>MM&nbsp;GeoIP</th>
                    <th>MM&nbsp;GeoIP2</th>
                </tr>
            </thead>
            <tbody>
        FOOBAR;
        $aIPs = array(
            $this->IP => (object)array(
                'expected' => '??',
                'context' => 'your current address',
            ),
            '137.221.105.2' => (object)array(
                'expected' => 'US',
                'context' => 'battle.net us west',
            ),
            '24.105.62.129' => (object)array(
                'expected' => 'US',
                'context' => 'battle.net us central',
            ),
            '211.115.104.1' => (object)array(
                'expected' => 'KR',
                'context' => 'battle.net korea',
            ),
            '51.116.168.114' => (object)array(
                'expected' => 'DE',
                'context' => 'Azure West Germany',
            ),
            '13.106.38.148' => (object)array(
                'expected' => 'US',
                'context' => 'Azure North Central US',
            ),
            '13.66.60.119' => (object)array(
                'expected' => 'US',
                'context' => 'Azure South Central US',
            ),
            '20.45.123.236' => (object)array(
                'expected' => 'US',
                'context' => 'Azure South Central US',
            ),
            '70.37.102.179' => (object)array(
                'expected' => 'US',
                'context' => 'Azure South Central US',
            ),
        );
        $oIP = new IP();
        $oPear = new PearLookup(__DIR__ . '/../db');
        $oPECL = new PECLLookup();
        $oMMGeoIP1 = new MaxMindV1Lookup(__DIR__ . '/../db');
        $oMMGeoIP2 = new MaxMindV2Lookup(__DIR__ . '/../db');
        foreach ($aIPs as $ip => $details) {
            $sCurrency = $oIP->lookupCountry($ip, '???', ['EUR', 'USD', 'KRW'])->currency;
            $oPearResult = $oPear->lookupCountry($ip);
            $sColorPear = (($oPearResult->code == $details->expected) || ($details->expected == '??')) ? 'green' : 'red';
            $oPECLResult = $oPECL->lookupCountry($ip);
            $sColorPECL = (($oPECLResult->code == $details->expected) || ($details->expected == '??')) ? 'green' : 'red';
            $oMMGeoIP2Result = $oMMGeoIP2->lookupCountry($ip);
            $sColorMMGeoIP2 = (($oMMGeoIP2Result->code == $details->expected) || ($details->expected == '??')) ? 'green' : 'red';
            $oMMGeoIP1Result = $oMMGeoIP1->lookupCountry($ip);
            $sColorMMGeoIP1 = (($oMMGeoIP1Result->code == $details->expected) || ($details->expected == '??')) ? 'green' : 'red';
            $ret .= <<<FOOBAR
            <tr>
            <td>{$ip}</td>
            <td>{$details->expected}</td>
            <td>{$details->context}</td>
            <td>{$sCurrency}</td>
            <td style="color: {$sColorPear}">{$oPearResult->code}</td>
            <td style="color: {$sColorPECL}">{$oPECLResult->code}</td>
            <td style="color: {$sColorMMGeoIP1}">{$oMMGeoIP1Result->code}</td>
            <td style="color: {$sColorMMGeoIP2}">{$oMMGeoIP2Result->code}</td>
            </tr>
            FOOBAR;
        }
        $ret .= <<<FOOBAR
            </tbody>
        </table>
        FOOBAR;
        return $ret;
    }

    protected function getSampleCode(): string
    {
        return <<<'FOOBAR'
        <h2>Sample Code</h2>
        <pre style="padding: 1em; border: 1px solid gainsboro; background-color: white;">
        $lookup = new \RedWood\GeoLookup\IP();
        $country = $lookup->lookupCountry($_SERVER['REMOTE_ADDR']);
        if ($country->code == 'US') {
            echo 'in the United States...';
        }
        print_r($country);
        </pre>
        This returns for example
        <pre style="padding: 1em; border: 1px solid gainsboro; background-color: white;">
        stdClass Object
        (
            [code] => DE
            [name] => Germany
            [success] => 1
            [source] => maxmind-geoip2
        )
        </pre>
        FOOBAR;
    }


    /**
     * Options page callback
     */
    public function createAdminPage()
    {
        $active_tab = (isset($_GET['tab'])) ? $_GET['tab'] : 'tests';
        echo <<<FOOBAR
        <div class="wrap">
            <h1>RedWood GeoLookup</h1>
        FOOBAR;
        echo '<h2 class="nav-tab-wrapper">';
        echo '<a href="/wp-admin/tools.php?page=geolookup&tab=tests" class="nav-tab ' . (($active_tab == 'tests') ? 'nav-tab-active' : '') . '">';
        echo __('Tests', 'redwood-geolookup');
        echo '</a>';
        echo '<a href="/wp-admin/tools.php?page=geolookup&tab=testmatrix" class="nav-tab ' . (($active_tab == 'testmatrix') ? 'nav-tab-active' : '') . '">';
        echo __('Test Matrix', 'redwood-geolookup');
        echo '</a>';
        echo '<a href="/wp-admin/tools.php?page=geolookup&tab=samplecode" class="nav-tab ' . (($active_tab == 'samplecode') ? 'nav-tab-active' : '') . '">';
        echo __('Sample Code', 'redwood-geolookup');
        echo '</a>';
        echo '</h2>';
        switch ($active_tab) {
            default:
            case 'tests':
                echo $this->getTests();
                break;
            case 'testmatrix':
                echo $this->getTestMatrix();
                break;
            case 'samplecode':
                echo $this->getSampleCode();
                break;
        }
        echo '</div>';
    }

    /**
     * Register and add settings
     */
    public function pageInit()
    {
    }
}
