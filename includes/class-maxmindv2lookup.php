<?php

/**
 * Specific class to look up country information using PECL libraries.
 *
 * @package    Redwood
 * @subpackage GeoLookup
 * @author     Patrick Kolla-ten Venne
 * @copyright  2023 Patrick Kolla-ten Venne
 * @license    MIT
 */

namespace RedWood\GeoLookup;

if (!class_exists('\GeoIp2\Database\Reader')) {
   require_once __DIR__ . '/geoip2.phar';
}

use \GeoIp2\Database\Reader;

class MaxMindV2Lookup
{
    protected $DatabasePath;
    protected $DatabaseFilename;
    public $DebugInformation = [];

    public function __construct(string $DatabasePath)
    {
        $this->DatabasePath = $DatabasePath;
        $this->DatabaseFilename = $this->DatabasePath . '/GeoLite2-Country.mmdb';
    }

    public function isSupported(): bool
    {
		$this->DebugInformation['FileExists']
			= json_encode(file_exists($this->DatabaseFilename));
		$this->DebugInformation['DatabaseFile']
			= $this->DatabaseFilename;
		$bIsSupported = (file_exists($this->DatabaseFilename));
		$this->DebugInformation['IsSupported']
			= json_encode($bIsSupported);
        return $bIsSupported;
    }

    public function lookupCountry($IPAddress): object
    {
		$a = array('code' => '', 'name' => '', 'success' => false, 'source' => 'maxmind-geoip2', 'currency' => '');
        if (!file_exists($this->DatabaseFilename)) {
			$a['errorCode'] = "Database file {$this->DatabaseFilename} missing.";
			$this->DebugInformation['ErrorCode'] = $a['errorCode'];
			return (object)$a;
		}
        try {
            $r = new Reader($this->DatabaseFilename);
            $record = $r->country($IPAddress);
            $a['code'] = $record->country->isoCode;
            $a['name'] = $record->country->name;
            $a['success'] = true;
            $this->DebugInformation['CountryCode'] = $a['code'];
            $this->DebugInformation['CountryName'] = $a['name'];
        } catch (\Exception $e) {
            // Handle exception
            $a['errorCode'] = $e->getMessage();
            $this->DebugInformation['ErrorCode'] = $a['errorCode'];
        }
        return (object)$a;
    }
}
