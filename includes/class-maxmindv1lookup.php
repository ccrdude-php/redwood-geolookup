<?php

/**
 * Specific class to look up country information using PECL libraries.
 *
 * @package    Redwood
 * @subpackage GeoLookup
 * @author     Patrick Kolla-ten Venne
 * @copyright  2023 Patrick Kolla-ten Venne
 * @license    MIT
 */

namespace RedWood\GeoLookup;

try {
    include_once 'geoip.php';
} catch (\Exception $e) {

}

class MaxMindV1Lookup
{
    protected $DatabasePath;
    protected $DatabaseFilename;
    public $DebugInformation = [];

    public function __construct(string $DatabasePath)
    {
        $this->DatabasePath = $DatabasePath;
        $this->DatabaseFilename = $this->DatabasePath . '/GeoIP.dat';
    }

    public function isSupported(): bool
    {
        $this->DebugInformation['OpenFunctionExists']
            = json_encode((function_exists('geoip_open')));
        $this->DebugInformation['NameFunctionExists']
            = json_encode((function_exists('geoip_country_name_by_addr')));
        $this->DebugInformation['CodeFunctionExists']
            = json_encode((function_exists('geoip_country_code_by_addr')));
        $bIsSupported = ((function_exists('geoip_open'))
            && (function_exists('geoip_country_name_by_addr'))
            && (function_exists('geoip_country_name_by_addr')));
        $this->DebugInformation['IsSupported']
            = json_encode($bIsSupported);
        return $bIsSupported;
    }

    public function lookupCountry($IPAddress): object
    {
		$a = array('code' => '', 'name' => '', 'success' => false, 'source' => 'maxmind-geoip1', 'currency' => '');
        if (!function_exists('geoip_open')) {
            $a['errorCode'] = 'Function geoip_open missing.';
            $this->DebugInformation['ErrorCode'] = $a['errorCode'];
            return (object)$a;
        }
        if (!function_exists('geoip_country_name_by_addr')) {
            $a['errorCode'] = 'Function geoip_country_name_by_addr missing.';
            $this->DebugInformation['ErrorCode'] = $a['errorCode'];
            return (object)$a;
        }
        if (!function_exists('geoip_country_code_by_addr')) {
            $a['errorCode'] = 'Function geoip_country_code_by_addr missing.';
            $this->DebugInformation['ErrorCode'] = $a['errorCode'];
            return (object)$a;
        }
        try {
            $gi = geoip_open($this->DatabaseFilename, GEOIP_STANDARD);
            $a['name'] = (geoip_country_name_by_addr($gi, $IPAddress));
            $a['code'] = (geoip_country_code_by_addr($gi, $IPAddress));
            geoip_close($gi);
            $a['success'] = true;
            $this->DebugInformation['CountryCode'] = $a['code'];
            $this->DebugInformation['CountryName'] = $a['name'];
        } catch (\Exception $e) {
            // Handle exception
            $a['errorCode'] = $e->getMessage();
            $this->DebugInformation['ErrorCode'] = $a['errorCode'];
        }
        return (object)$a;
    }
}
