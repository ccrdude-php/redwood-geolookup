<?php
/**
 * Specific class to look up country information using Pear libraries.
 *
 * @package    Redwood
 * @subpackage GeoLookup
 * @author     Patrick Kolla-ten Venne
 * @copyright  2023 Patrick Kolla-ten Venne
 * @license    MIT
 */

namespace RedWood\GeoLookup;

include_once 'Net/GeoIP.php';

class PearLookup
{
	protected $DatabasePath;
	protected $DatabaseFilename;
	public $DebugInformation = [];

	public function __construct(string $DatabasePath)
	{
		$this->DatabasePath = $DatabasePath;
		$this->DatabaseFilename = $this->DatabasePath . '/GeoIP.dat';
	}

	public function isSupported(): bool
	{
		$this->DebugInformation['ClassExists']
			= json_encode(class_exists('Net_GeoIP'));
		$this->DebugInformation['FileExists']
			= json_encode(file_exists($this->DatabaseFilename));
		$this->DebugInformation['DatabaseFile']
			= $this->DatabaseFilename;
		$bIsSupported = ((class_exists('Net_GeoIP'))
			&& (file_exists($this->DatabaseFilename)));
		$this->DebugInformation['IsSupported']
			= json_encode($bIsSupported);
		return $bIsSupported;
	}

	public function lookupCountry($IPAddress): object
	{
		$a = array('code' => '', 'name' => '', 'success' => false, 'source' => 'pear', 'currency' => '');
		if (!class_exists('Net_GeoIP')) {
			$a['errorCode'] = 'Class Net_GeoIP missing.';
			$this->DebugInformation['ErrorCode'] = $a['errorCode'];
			return (object)$a;
		}
		if (!file_exists($this->DatabaseFilename)) {
			$a['errorCode'] = "Database file {$this->DatabaseFilename} missing.";
			$this->DebugInformation['ErrorCode'] = $a['errorCode'];
			return (object)$a;
		}
		$geoip = \Net_GeoIP::getInstance($this->DatabaseFilename, \Net_GeoIP::STANDARD);
		try {
			$a['code'] = $geoip->lookupCountryCode($IPAddress);
			$a['name'] = $geoip->lookupCountryName($IPAddress);
			$a['success'] = true;
			$this->DebugInformation['CountryCode'] = $a['code'];
			$this->DebugInformation['CountryName'] = $a['name'];
		} catch (\Exception $e) {
			// Handle exception
			$a['errorCode'] = $e->getMessage();
			$this->DebugInformation['ErrorCode'] = $a['errorCode'];
		}
		return (object)$a;
	}
}
