# RedWood GeoLookup

This WordPress plugin can be used to look up geo information on IP addresses.

To do this, it supports four different lookup APIs:

 - [Pear Net_geoIP](https://pear.php.net/package/Net_GeoIP) (last updated 2011)
 - [PECL geoip](http://pecl.php.net/package/GEOIP) (last updated 2016)
 - [MaxMind GeoIP Legacy](https://github.com/maxmind/geoip-api-php) (retired May 2022)
 - [MaxMind GeoIP v2](https://github.com/maxmind/GeoIP2-php)

## RedWood

RedWood is my main namespace for WordPress plugins.

## Sample Code

```php
$lookup = new \RedWood\GeoLookup\IP();
$country = $lookup->lookupCountry($_SERVER['REMOTE_ADDR']);
if ($country->code == 'US') {
    echo 'in the United States...';
}
print_r($country);
```

This returns for example:

```
stdClass Object
(
    [code] => DE
    [name] => Germany
    [success] => 1
    [source] => maxmind-geoip2
)
```